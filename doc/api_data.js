define({ "api": [
  {
    "type": "get",
    "url": "/healthcheck",
    "title": "get health of system",
    "name": "Healthcheck",
    "group": "System",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "result",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.status",
            "description": "<p>The status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/healthcheck"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/health.js",
    "groupTitle": "System"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "login api.",
    "name": "auth",
    "group": "login",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "token",
            "description": "<p>object</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n  \"user\": {\n    \"_id\": \"590463063fc4983aa2156435\",\n    \"email\": \"aman@fourzip.com\"\n  },\n  \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1OTA0NjMwNjNmYzQ5ODNhYTIxNTY0MzUiLCJpYXQiOjE0OTM3MzE3ODEsImV4cCI6MTQ5MzgxODE4MX0.-spUI8SZKECmpuQSzGD2nfF7UmQ3u-by50INr9HbAS4\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/login"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/login.js",
    "groupTitle": "login"
  },
  {
    "type": "post",
    "url": "/seo",
    "title": "create a seo.",
    "name": "create_a_seo",
    "group": "seo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "request-example :",
          "content": "{\n     \"cus_id\": \"\",\n     \"type\": \"article\",\n     \"cannonical_uri\": \"\",\n     \"isCannonical\": false,\n     \"meta_robot_tags\": \"\",\n     \"page_uri\": \"\",\n     \"keywords\": \"\",\n     \"description\": \"\",\n     \"title\": \"why this kolaveri D\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/seo"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"article\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  },
  {
    "type": "get",
    "url": "/seo/articles/:id",
    "title": "get article by id.",
    "name": "get_article_by_id",
    "group": "seo",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>unique id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>created date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"article\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/seo/articles/ARTICLE_ID"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  },
  {
    "type": "get",
    "url": "/seo/articles",
    "title": "get all articles",
    "name": "get_articles",
    "group": "seo",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>unique id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>created date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  [\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"article\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }]",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/seo/articles"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  },
  {
    "type": "get",
    "url": "/seo/industrys",
    "title": "get all industries seo",
    "name": "get_industries",
    "group": "seo",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>unique id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>created date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  [\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"tags\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }]",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/seo/industrys"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  },
  {
    "type": "get",
    "url": "/seo/industrys/:id",
    "title": "get indsutries by id.",
    "name": "get_industry_by_id",
    "group": "seo",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>unique id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>created date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"tag\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/seo/industrys/Industry_ID"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  },
  {
    "type": "get",
    "url": "/seo/:id",
    "title": "get seo by id.",
    "name": "get_seo_by_id",
    "group": "seo",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>unique id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>created date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"article\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/seo/SEO_ID"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  },
  {
    "type": "get",
    "url": "/seo/tags/:id",
    "title": "get tags by id.",
    "name": "get_tag_by_id",
    "group": "seo",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>unique id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>created date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"tag\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/seo/articles/ARTICLE_ID"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  },
  {
    "type": "get",
    "url": "/seo/tagss",
    "title": "get all tags seo",
    "name": "get_tags",
    "group": "seo",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>unique id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>created date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  [\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"tags\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }]",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/seo/tags"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  },
  {
    "type": "get",
    "url": "/seo/users/:id",
    "title": "get users by id.",
    "name": "get_user_by_id",
    "group": "seo",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>unique id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>created date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"article\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/seo/articles/ARTICLE_ID"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  },
  {
    "type": "get",
    "url": "/seo/users",
    "title": "get all users",
    "name": "get_users",
    "group": "seo",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>unique id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>created date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  [\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"article\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }]",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/seo/users"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  },
  {
    "type": "get",
    "url": "/seo",
    "title": "get all seo",
    "name": "seo_get",
    "group": "seo",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>unique id</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>created date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cus_id",
            "description": "<p>custom id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of seo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cannonical_uri",
            "description": "<p>cannonical uri</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isCannonical",
            "description": "<p>boolean filed.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meta_robot_tags",
            "description": "<p>meta robot tags</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "page_uri",
            "description": "<p>page uri.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  [\n{\n  \"_id\": \"59038e6deaa935a6be2563e0\",\n  \"__v\": 0,\n  \"created\": \"2017-04-28T18:48:13.566Z\",\n  \"cus_id\": \"\",\n  \"type\": \"article\",\n  \"cannonical_uri\": \"\",\n  \"isCannonical\": false,\n  \"meta_robot_tags\": \"\",\n  \"page_uri\": \"\",\n  \"keywords\": \"\",\n  \"description\": \"\",\n  \"title\": \"why this kolaveri D\"\n }]",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer Token.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/seo"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/seo.js",
    "groupTitle": "seo"
  }
] });

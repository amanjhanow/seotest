// ensure the NODE_ENV is set to 'test'
// this is helpful when you would like to change behavior when testing
process.env.NODE_ENV = 'test';

var express = require('express');
var router = express.Router();


var authContoller = require('../controllers/auth');
/**
 * @api {post} /login   login api.
 * @apiName auth
 * @apiGroup login
 *
 * @apiParam {String} email email id.
 * @apiParam {String} password password.
 * 
 * @apiSuccess {Object}   token object  
 * @apiSampleRequest /login
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
{
  "user": {
    "_id": "590463063fc4983aa2156435",
    "email": "aman@fourzip.com"
  },
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1OTA0NjMwNjNmYzQ5ODNhYTIxNTY0MzUiLCJpYXQiOjE0OTM3MzE3ODEsImV4cCI6MTQ5MzgxODE4MX0.-spUI8SZKECmpuQSzGD2nfF7UmQ3u-by50INr9HbAS4"
}
 */
router.post('/', authContoller.userAuth);


module.exports = router;

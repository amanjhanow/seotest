// ensure the NODE_ENV is set to 'test'
// this is helpful when you would like to change behavior when testing
process.env.NODE_ENV = 'test';

var express = require('express');
var router = express.Router();

var SeoController = require('../controllers/seo');
const isAuthorized = require('../middlewares/isAuthorized');
/**
 * @api {get} /seo      get all seo
 * @apiName seo_get
 * @apiGroup seo
 *
 * @apiSuccess {String} _id unique id
 * @apiSuccess {Date} created created date
 * @apiSuccess {String} cus_id custom id
 * @apiSuccess {String} type type of seo
 * @apiSuccess {String} cannonical_uri cannonical uri
 * @apiSuccess {Boolean} isCannonical boolean filed.
 * @apiSuccess {String} meta_robot_tags meta robot tags
 * @apiSuccess {String} page_uri page uri.
 * @apiSuccess {String} keywords keywords.
 * @apiSuccess {String} description description.
 * @apiSuccess {String} title title
 *
 * @apiHeader {String} Authorization Bearer Token.
 * @apiSampleRequest /seo
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "article",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }]
 */

router.get('/', isAuthorized, function(req, res) {

    SeoController.findAll(function(err, seo) {
        if (err) {
            return res.status(400).json({error: err});
        }

        return res.json(seo);
    });

});



/**
 * @api {get} /seo/articles   get all articles
 * @apiName get_articles
 * @apiGroup seo
 *
 * @apiSuccess {String} _id unique id
 * @apiSuccess {Date} created created date
 * @apiSuccess {String} cus_id custom id
 * @apiSuccess {String} type type of seo
 * @apiSuccess {String} cannonical_uri cannonical uri
 * @apiSuccess {Boolean} isCannonical boolean filed.
 * @apiSuccess {String} meta_robot_tags meta robot tags
 * @apiSuccess {String} page_uri page uri.
 * @apiSuccess {String} keywords keywords.
 * @apiSuccess {String} description description.
 * @apiSuccess {String} title title
 *
 * @apiSampleRequest /seo/articles
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "article",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }]
 */



router.get('/articles', function(req,res) {

    SeoController.findArticle(function(err, article) {
        if (err) {
            return res.status(400).json({error: err});
        }

        return res.json(article);
    })
});



/**
 * @api {get} /seo/users   get all users
 * @apiName get_users
 * @apiGroup seo
 *
 * @apiSuccess {String} _id unique id
 * @apiSuccess {Date} created created date
 * @apiSuccess {String} cus_id custom id
 * @apiSuccess {String} type type of seo
 * @apiSuccess {String} cannonical_uri cannonical uri
 * @apiSuccess {Boolean} isCannonical boolean filed.
 * @apiSuccess {String} meta_robot_tags meta robot tags
 * @apiSuccess {String} page_uri page uri.
 * @apiSuccess {String} keywords keywords.
 * @apiSuccess {String} description description.
 * @apiSuccess {String} title title
 *
 * @apiSampleRequest /seo/users
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "article",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }]
 */

router.get('/users', function(req,res) {

    SeoController.findUser(function(err, users) {
        if (err) {
            return res.status(400).json({error: err});
        }

        return res.json(users);
    })
});

/**
 * @api {get} /seo/tagss   get all tags seo
 * @apiName get_tags
 * @apiGroup seo
 *
 * @apiSuccess {String} _id unique id
 * @apiSuccess {Date} created created date
 * @apiSuccess {String} cus_id custom id
 * @apiSuccess {String} type type of seo
 * @apiSuccess {String} cannonical_uri cannonical uri
 * @apiSuccess {Boolean} isCannonical boolean filed.
 * @apiSuccess {String} meta_robot_tags meta robot tags
 * @apiSuccess {String} page_uri page uri.
 * @apiSuccess {String} keywords keywords.
 * @apiSuccess {String} description description.
 * @apiSuccess {String} title title
 *
 * @apiSampleRequest /seo/tags
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "tags",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }]
 */

router.get('/tags', function(req,res) {

    SeoController.findTag(function(err, tags) {
        if (err) {
            return res.status(400).json({error: err});
        }

        return res.json(tags);
    })
});

/**
 * @api {get} /seo/industrys   get all industries seo
 * @apiName get_industries
 * @apiGroup seo
 *
 * @apiSuccess {String} _id unique id
 * @apiSuccess {Date} created created date
 * @apiSuccess {String} cus_id custom id
 * @apiSuccess {String} type type of seo
 * @apiSuccess {String} cannonical_uri cannonical uri
 * @apiSuccess {Boolean} isCannonical boolean filed.
 * @apiSuccess {String} meta_robot_tags meta robot tags
 * @apiSuccess {String} page_uri page uri.
 * @apiSuccess {String} keywords keywords.
 * @apiSuccess {String} description description.
 * @apiSuccess {String} title title
 *
 * @apiSampleRequest /seo/industrys
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "tags",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }]
 */

router.get('/industrys', function(req,res) {

    SeoController.findIndustry(function(err, industries) {
        if (err) {
            return res.status(400).json({error: err});
        }

        return res.json(industries);
    })
});


/**
 * @api {get} /seo/articles/:id   get article by id.
 * @apiName get_article_by_id
 * @apiGroup seo
 *
 * @apiSuccess {String} _id unique id
 * @apiSuccess {Date} created created date
 * @apiSuccess {String} cus_id custom id
 * @apiSuccess {String} type type of seo
 * @apiSuccess {String} cannonical_uri cannonical uri
 * @apiSuccess {Boolean} isCannonical boolean filed.
 * @apiSuccess {String} meta_robot_tags meta robot tags
 * @apiSuccess {String} page_uri page uri.
 * @apiSuccess {String} keywords keywords.
 * @apiSuccess {String} description description.
 * @apiSuccess {String} title title
 *
 * @apiSampleRequest /seo/articles/ARTICLE_ID
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "article",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }
 */


router.get('/articles/:id', function(req, res) {
    var article_id = req.params.id;
    SeoController.findArticleById(article_id, function(err, article) {
        if (err) {
            return res.status(400).json( { error: err});
        }

        return res.json(article);
    })
});


/**
 * @api {get} /seo/users/:id   get users by id.
 * @apiName get_user_by_id
 * @apiGroup seo
 *
 * @apiSuccess {String} _id unique id
 * @apiSuccess {Date} created created date
 * @apiSuccess {String} cus_id custom id
 * @apiSuccess {String} type type of seo
 * @apiSuccess {String} cannonical_uri cannonical uri
 * @apiSuccess {Boolean} isCannonical boolean filed.
 * @apiSuccess {String} meta_robot_tags meta robot tags
 * @apiSuccess {String} page_uri page uri.
 * @apiSuccess {String} keywords keywords.
 * @apiSuccess {String} description description.
 * @apiSuccess {String} title title
 *
 * @apiSampleRequest /seo/articles/ARTICLE_ID
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "article",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }
 */

router.get('/users/:id', function(req, res) {
    var user_id = req.params.id;
    SeoController.findUserById(user_id, function(err, user) {
        if (err) {
            return res.status(400).json( { error: err});
        }

        return res.json(user);
    })

})

/**
 * @api {get} /seo/tags/:id   get tags by id.
 * @apiName get_tag_by_id
 * @apiGroup seo
 *
 * @apiSuccess {String} _id unique id
 * @apiSuccess {Date} created created date
 * @apiSuccess {String} cus_id custom id
 * @apiSuccess {String} type type of seo
 * @apiSuccess {String} cannonical_uri cannonical uri
 * @apiSuccess {Boolean} isCannonical boolean filed.
 * @apiSuccess {String} meta_robot_tags meta robot tags
 * @apiSuccess {String} page_uri page uri.
 * @apiSuccess {String} keywords keywords.
 * @apiSuccess {String} description description.
 * @apiSuccess {String} title title
 *
 * @apiSampleRequest /seo/articles/ARTICLE_ID
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "tag",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }
 */

router.get('/tags/:id', function(req, res) {
    var tag_id = req.params.id;
    SeoController.findTagById(tag_id, function(err, tag) {
        if (err) {
            return res.status(400).json( { error: err});
        }

        return res.json(tag);
    })

})

/**
 * @api {get} /seo/industrys/:id   get indsutries by id.
 * @apiName get_industry_by_id
 * @apiGroup seo
 *
 * @apiSuccess {String} _id unique id
 * @apiSuccess {Date} created created date
 * @apiSuccess {String} cus_id custom id
 * @apiSuccess {String} type type of seo
 * @apiSuccess {String} cannonical_uri cannonical uri
 * @apiSuccess {Boolean} isCannonical boolean filed.
 * @apiSuccess {String} meta_robot_tags meta robot tags
 * @apiSuccess {String} page_uri page uri.
 * @apiSuccess {String} keywords keywords.
 * @apiSuccess {String} description description.
 * @apiSuccess {String} title title
 *
 * @apiSampleRequest /seo/industrys/Industry_ID
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "tag",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }
 */

router.get('/industrys/:id', function(req, res) {
    var industry_id = req.params.id;
    SeoController.findIndustryById(industry_id, function(err, industry) {
        if (err) {
            return res.status(400).json( { error: err});
        }

        return res.json(industry);
    })

})

/**
 * @api {get} /seo/:id   get seo by id.
 * @apiName get_seo_by_id
 * @apiGroup seo
 *
 * @apiSuccess {String} _id unique id
 * @apiSuccess {Date} created created date
 * @apiSuccess {String} cus_id custom id
 * @apiSuccess {String} type type of seo
 * @apiSuccess {String} cannonical_uri cannonical uri
 * @apiSuccess {Boolean} isCannonical boolean filed.
 * @apiSuccess {String} meta_robot_tags meta robot tags
 * @apiSuccess {String} page_uri page uri.
 * @apiSuccess {String} keywords keywords.
 * @apiSuccess {String} description description.
 * @apiSuccess {String} title title
 *
 * @apiSampleRequest /seo/SEO_ID
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "article",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }
 */

router.get('/:id',isAuthorized , function(req, res) {
    var seo_id = req.params.id;
    SeoController.findSeoById(seo_id, function(err, seo) {
        if (err) {
            return res.status(400).json( { error: err});
        }

        return res.json(seo);
    })
})

/**
 * @api {post} /seo   create a seo.
 * @apiName create a seo
 * @apiGroup seo
 *

 * @apiParam {String} cus_id custom id
 * @apiParam {String} type type of seo
 * @apiParam {String} cannonical_uri cannonical uri
 * @apiParam {Boolean} isCannonical boolean filed.
 * @apiParam {String} meta_robot_tags meta robot tags
 * @apiParam {String} page_uri page uri.
 * @apiParam {String} keywords keywords.
 * @apiParam {String} description description.
 * @apiParam {String} title title
 *
 * @apiSampleRequest /seo
 *
 * @apiParamExample {json} request-example :
   {
        "cus_id": "",
        "type": "article",
        "cannonical_uri": "",
        "isCannonical": false,
        "meta_robot_tags": "",
        "page_uri": "",
        "keywords": "",
        "description": "",
        "title": "why this kolaveri D"
   }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
  {
    "_id": "59038e6deaa935a6be2563e0",
    "__v": 0,
    "created": "2017-04-28T18:48:13.566Z",
    "cus_id": "",
    "type": "article",
    "cannonical_uri": "",
    "isCannonical": false,
    "meta_robot_tags": "",
    "page_uri": "",
    "keywords": "",
    "description": "",
    "title": "why this kolaveri D"
   }
 */
router.post('/', isAuthorized ,function(req, res) {

    SeoController.create(req.body, function(err, result) {
        if (err) {
            return res.status(400).json({error: err});
        }

        return res.json(result);
    });
});

module.exports = router;

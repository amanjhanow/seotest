var mongoose = require('mongoose');

/**
 * Example schema
 */

const seoSchema = new mongoose.Schema({
    title: { type: String, default: '', trim: true },
    description: { type: String, default: '', trim: true },
    keywords: { type: String, default: '', trim: true },
    page_uri: { type: String, default: '', trim: true },
    meta_robot_tags: { type: String, default: '', trim: true },
    isCannonical: { type: Boolean, default: false },
    cannonical_uri: { type: String, default: '', trim: true },
    type: { type: String, default: '', trim: true },
    cus_id: { type: String, default: '', trim: true, unique : true},
    created: {
        type: Date,
        default: Date.now
    }
});

/**
 * Events
 */

// on every save, add the dates
seoSchema.pre('save', function (next) {
    this.created = Date.now;
    next();
});

/**
 * Statics
 */

/**
 * Find a user.
 *
 * @param {String} username
 * @param {Function} callback
 * @api public
 * @static
 */
// exampleSchema.statics.getUserByUsername = function(username, callback) {
//     return this.where('username', username).exec(callback);
// };

/**
 * Exports
 */
module.exports = mongoose.model('SeoModel', seoSchema);
